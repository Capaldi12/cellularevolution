"""
    В этом файле находится вся логика клеток и поля
    Cell - клетка, единица жизни в игре
    Field - поле действия, лента, закольцованная по вертикальным граням
"""

from functions import *

# Константы //Скорее всего будут вынесены в отдельный файл, но это не точно
# Часть их них будет вводиться пользователем (отмечены |>)
MAXE = 999  # maximum energy        - максимальная энергия любой клетки
MAXM = 499  # maximum mineral       - максимальное количество минералов у клетки

GSmin = 64  # минимальный размер генома. Меньше никак нельзя

GS = 100    # |> genome size        - размер генома
AL = 18     # |> action limit       - лимит незавершающих действий за ход
MC = 0.4    # |> mutation chance    - шанс мутации
MM = 4      # |> mutation magnitude - величина мутации
INIT = 42   # ген первой клетки

WALL = -10      # Код стены
EMPTY = -1      # Код пустоты

LEVELS = 25     # |> Количество "энергетических уровней"
TOP_LVL = 15    # |> Значение "энергии" на максимальном уровне

NUTRITION = 50  # |> Питательность мертвой клетки
FR = .5         # |> Доля энергии противника, которую получает клетка при съедении
REPEN = 100     # |> Количество энергии, необходимое для размножения
MIN = 1         # |> Количество энергии на один минерал

GENSEED = None  # |> "Семя" для генератора случайных чисел


# Клетка - ИИ-управляемый персонаж
# Клетки живут на поле, питаются органикой/минералами/солнечным светом
# Размножаются почкованием, меняют цвет в зависимости от рациона
# Обладают геномом из GS генов, гены определяют поведение
# Геном является своеобразной программой, каждый ген кодирует действие или условный/безусловный переход
# Каждый ход указатель проходит по геному, пока не встретит "завершающее" действие (например шаг)
# или пока не достигнет лимита действий за ход. Действия расходуют энергию, переходы нет, но добавляют к счетчику действий
class Cell:
    # field - поле, где живет клетка
    # pos - позиция, где будет сидеть клетка
    # parent - клетка-родитель
    # multi - многокеточная ли структура
    # init_gene - начальный ген, которым заполняется геном. -1 для случайного задания
    def __init__(self, field, _id, pos=(0, 0), parent=None, multi=False, init=-1):
        self.genome = []        # Геном клетки - алгоритм действий
        self.pointer = 0        # Номер следующего гена

        self.id = _id

        self.energy = 0         # Энергия (здоровье) клетки - если она закончится, клетка умрет
        self.minerals = 0       # Запас минералов клетки - влияют на прочность и силу клетки, можно кушать
        self.x, self.y = pos

        self.direction = 0      # Направление взгляда
        self.dead = False
        self.color = (255, 255, 255)  # Цвет клетки

        # Указатели на следующую и предыдущую клетку для многоклеточной структуры (будем делиться энергией. потом)
        self.previous = None
        self.next = None

        self.field = field      # Указатель на поле

        if parent is not None:    # Если задан родитель - повторяем геном, возможно мутируем
            if multi:   # Прилепляемся к родителю
                self.previous = parent

            self.energy = parent.energy // 2    # Энергия в половину родительской
            self.genome = list(parent.genome)   # Копируем геном родителя
            self.color = parent.color

            chance = randint(1, 100)    # Число определяющее, будет ли мутация
            if chance <= MC*10:             # Мутация с заданным шансом
                mutation = rng(len(self.genome))    # Выбираем случайный ген
                amount = randint(-MM, MM)       # Выбираем случайный прирост

                # Прибавляем прирост, но не даем выйти за 0 или MG (таких генов не бывает)
                new = self.genome[mutation] + amount
                new = new if new >= 0 else new + GS
                new = new if new <= GS else new - GS
                self.genome[mutation] = new

        else:   # Если родителя нету (самая первая клетка) - генерируем геном случайно или заполняем одним геном
            self.energy = 50 + randint(-5, 5)    # Энергия 50+-5
            if init == -1:     # Случайный геном
                self.genome = [rng(GS) for _ in range(GS)]
            else:                   # Один ген
                self.genome = [init] * GS

    # Делаем наш ход. Смотрим на текщий ген и выбираем действие
    def makeTurn(self):
        if not self.dead:
            actions = 0
            self.energy -= 1

            while 0 <= actions < AL and not self.dead:    # Пока не достигли лимита и не получили сигнал на выход (-1)
                actions += 1
                gene = self.genome[self.pointer]  # Получаем текущий ген

                # Выбираем соответствуещее действие
                if gene == 42:
                    dis = self.photosynthesis()
                    actions = -1
                elif gene == 43:
                    dis = self.chemosynthesis()
                    actions = -1
                elif gene == 45:
                    dis = self.turnRelative()
                elif gene == 46:
                    dis = self.turnAbsolute()
                elif gene == 38:
                    dis = self.step()
                    actions = -1
                elif gene == 40:
                    dis = self.bite()
                    actions = -1
                elif gene == 62:
                    dis = self.reproduct()
                    actions = -1
                elif gene == 63:
                    dis = self.reproductMulti()
                    actions = -1
                elif gene == 55:
                    dis = self.whatAmILookingAt()
                elif gene == 56:
                    dis = self.amISurrounded()
                elif gene == 59:
                    dis = self.howIsEnergy()
                elif gene == 60:
                    dis = self.howIsMinerals()
                elif gene == 32:
                    dis = self.isItSunny()
                elif gene == 31:
                    dis = self.isThereMinerals()
                else:
                    dis = gene if gene != 0 else 1  # Безусловный переход, но убеждаемся, что не застрянем на нуле
                    # Стоит ли запретить ноль? или назначить команду на него?

                self.movePointer(dis)   # Перемещаем указатель в зависимости от результата

                if self.energy <= 0:  # Не забываем умереть, если вдруг закончилась энергия
                    self.die()

            self.absorbMinerals()   # В конце хода автоматически поглощаем минералы

            if self.energy >= MAXE:     # По достижении максимума энергии делаем потомка
                self.reproduct()
        else:
            self.sink()

    # --- SERVICE ---
    # Это блок всяких проверок и воспомогательных действий

    # Перемещаем указатель по ленте генома
    def movePointer(self, amount):
        self.pointer += amount
        if self.pointer >= GS:
            self.pointer -= GS

    # Получаем ген из генома, учитывая зацикленность
    def getGene(self, number):
        if number >= GS:
            number -= GS

        return self.genome[number]

    # Проверка на многоклеточность
    def isMulti(self):
        return isinstance(self.previous, Cell) or isinstance(self.next, Cell)

    # Превращаемся в трупик
    def die(self):
        self.dead = True
        if self.previous is not None:
            self.previous.next = None
            self.previous = None
        if self.next is not None:
            self.next.previous = None
            self.next = None

    # Тонем, если трупик
    def sink(self):
        x, y = self.x + 1, self.y
        if self.field.whatsThere((x, y)) is EMPTY:
            self.field.moveCell(self, (x, y))

    # Изменяем цвет клетки
    def goRed(self, magnitude=10):
        self.color = addColors(self.color, (magnitude, -magnitude, -magnitude))

    def goGreen(self, magnitude=10):
        self.color = addColors(self.color, (-magnitude, magnitude, -magnitude))

    def goBlue(self, magnitude=10):
        self.color = addColors(self.color, (-magnitude, -magnitude, magnitude))

    # Запасаем минералы
    def absorbMinerals(self):
        self.minerals += self.field.howIsMineral(self.x)
        if self.minerals > MAXM:
            self.minerals = MAXM

    # Покусать противника
    def fight(self, victim):
        energy = 0
        if self.minerals >= victim.minerals:     # Если наши зубы крепче его брони
            self.minerals -= victim.minerals     # Зубы частично стачиваются
            energy = NUTRITION + FR * victim.energy  # А клетка съедена
            self.field.disposeCell(victim.id)
        else:
            victim.minerals -= self.minerals     # Его броня частично сточилась
            self.minerals = 0               # А наши зубы целиком

            # Пытаемся пробить оставшуюся защиту грубой силой
            if self.energy >= victim.minerals * 2:   # Проломили броню и съели, но сами поранились
                energy = NUTRITION + FR * victim.energy - 2 * victim.minerals
                self.field.disposeCell(victim.id)
            else:      # Погибаем смертью храбрых
                victim.minerals -= self.energy
                self.die()

        # Получаем энергию и краснеем
        self.energy += energy
        self.goRed(energy)

    # Проверяем, родственник ли данная клетка
    # На данный момент родственник - прямой предок или потомок
    # В дальнейшем планируется сравнение генома (меньше x отличий, значит родственник)
    def isMyRelative(self, cell):
        return cell is self.previous or cell is self.next

    # Выбор свободной позиции, куда сделаем потомка
    def reproductionPos(self):
        ways = [(self.x - 1, self.y), (self.x + 1, self.y), (self.x, self.y - 1), (self.x, self.y + 1)]
        for _ in range(4):
            way = rng(len(ways))
            x, y = ways.pop(way)
            if self.field.whatsThere((x, y)) is EMPTY:
                return x, y
        return None

    # Получаем координаты на которые смотрим в данный момент
    def whereAmILooking(self):
        _dir = self.direction
        new_x = self.x
        new_y = self.y

        # Выбираем новую позицию по сетке направлений
        #   7 0 1
        #   6 Δ 2
        #   5 4 3
        if _dir in [0, 1, 7]:
            new_x += 1
        if _dir in [3, 4, 5]:
            new_x -= 1
        if _dir in [1, 2, 3]:
            new_y += 1
        if _dir in [5, 6, 7]:
            new_y -= 1

        return new_x, new_y

    # --- JUMPS ---
    # Тут условные переходы. Будут

    # Проверяем, на что смотрим в данный момент
    def whatAmILookingAt(self):
        self.energy -= 1    # Мышление тратит энергию

        x, y = self.whereAmILooking()

        # Узнаем у поля, что в этой позиции
        what = self.field.whatsThere((x, y))

        if what is EMPTY:
            return 1  # Тут пусто
        elif what is WALL:
            return 2  # Стенка
        else:
            c = self.field.cells[what]
            if c.dead:
                return 3  # Трупик
            elif self.isMyRelative(c):
                return 4  # Родственник
            else:
                return 5  # Незнакомец

    # Проверяем, окружены ли мы
    def amISurrounded(self):
        x = self.x
        y = self.y
        if self.field.whatsThere(x-1, y) is EMPTY or self.field.whatsThere(x+1, y) is EMPTY \
                or self.field.whatsThere(x, y-1) is EMPTY or self.field.whatsThere(x, y+1) is EMPTY:
            return 3
        else:
            return 2

    # Проверяем, сколько у нас энергии
    def howIsEnergy(self):
        nxt = self.getGene(self.pointer + 1)
        if self.energy < 10 * nxt:
            return 2
        else:
            return 3

    # Проверяем, сколько у нас минералов
    def howIsMinerals(self):
        nxt = self.getGene(self.pointer + 1)
        if self.minerals < 5 * nxt:
            return 2
        else:
            return 3

    # Проверяем, солнечно ли вокруг
    def isItSunny(self):
        nxt = self.getGene(self.pointer + 1)
        if self.field.howIsSun(self.x) < nxt % 15:
            return 2
        else:
            return 3

    # Проверяем, есть ли рядом минералы
    def isThereMinerals(self):
        nxt = self.getGene(self.pointer + 1)
        if self.field.howIsMineral(self.x) < nxt % 15:
            return 2
        else:
            return 3

    # --- ACTIONS ---
    # Этот блок активных действий клетки

    # Поворот относительно текущего положения
    def turnRelative(self):
        self.energy -= 1

        self.direction += self.getGene(self.pointer + 1) % 8
        if self.direction > 8:
            self.direction -= 8

        return 2

    # Поворот безотносительно текущего положения
    def turnAbsolute(self):
        self.energy -= 1

        self.direction = self.getGene(self.pointer + 1) % 8

        return 2

    # Шаг вперед, если можно, с возвратом "результата"
    # в зависимости от того, что было в целевой позиции
    def step(self):
        self.energy -= 1
        x, y = self.whereAmILooking()

        # Узнаем у поля, что в этой позиции
        what = self.field.whatsThere((x, y))

        if what is EMPTY:   # Шагаем, если пусто
            if not self.isMulti():  # Многоклеточные не пермещаются
                self.energy -= 4    # Немного устаем
                self.field.moveCell(self, (x, y))
            return 1
        elif what is WALL:
            return 2    # Игнорим стенку
        else:
            c = self.field.cells[what]
            if c.dead:
                return 3    # Чтим мертвого собрата
            elif self.isMyRelative(c):
                return 4    # Обращаем внимание на родственника
            else:
                return 5    # Смотрим в глаза незнакомцу

    # Кусаем вперед, если возможно, запоминаем содержимое "укушенной" клетки
    def bite(self):
        self.energy -= 4

        # Узнаем у поля, что в этой позиции
        x, y = self.whereAmILooking()
        what = self.field.whatsThere((x, y))

        if what is EMPTY:
            return 1        # Пустоту не покусаешь
        elif what is WALL:
            return 2        # Стенка тоже жесткая
        else:
            c = self.field.cells[what]
            if c.dead:
                self.energy += self.field.disposeCell(what)
                self.goRed(NUTRITION)
                return 3        # Скушали мясо
            elif self.isMyRelative(c):
                return 4        # Друзей не едят
            else:   # И тут я начинаю кусать
                self.fight(c)
                return 5

    # Получаем энергию от солнца
    def photosynthesis(self):
        sun = self.field.howIsSun(self.x)
        self.energy += sun
        self.goGreen(sun)

        return 1

    # Преобразуем минералы в энергию
    def chemosynthesis(self):
        if self.minerals >= TOP_LVL:
            am = TOP_LVL * MIN
            self.minerals -= TOP_LVL
        else:
            am = self.minerals * MIN
            self.minerals = 0

        self.energy += am
        self.goBlue(am)

        return 1

    # Делаем потомка
    def reproduct(self):
        if self.energy >= REPEN:
            pos = self.reproductionPos()  # Выбираем позицию, если такая есть
            if pos is not None:
                self.field.putCell(pos, self, multi=False)   # Отпочковываем свободную клетку
                self.energy -= self.energy // 2     # Отдаем энергию
            else:
                self.die()  # Клетка не выносит позора и делает харакири
        else:
            self.die()  # Клетка черезмерно истощается и погибает, не дав потомка
        return 1

    # Деалаем потомка и цепляем его к себе
    def reproductMulti(self):
        if self.next is None:  # Если клетка не имеет прикрепленного потомка
            if self.energy >= REPEN:
                pos = self.reproductionPos()  # Выбираем позицию, если такая есть
                if pos is not None:
                    heir = self.field.putCell(pos, self, multi=True)   # Отпочковываем свободную клетку
                    self.next = heir  # Запоминаем её
                    self.energy -= self.energy // 2     # Отдаем энергию
                else:
                    self.die()  # Клетка не выносит позора и делает харакири
            else:
                self.die()  # Клетка черезмерно истощается и погибает, не дав потомка
        # Если прикрепленный потомок есть, то ничего не делаем
        return 1


# Игровое поле - прямоугольник H на W, на котором происходит всё действие
# На поле присутствуют солнце и минералы (как параметры клеток)
# Поле разделено на LEVELS "энергоуровней" независимо для них
# Солнце начинается сверху и убывает вниз
# Минералы начиаются снизу и убывают вверх
# В центре они пересекаются(или нет, если LEVELS > 2*TOP_LEVEL)
# Солнце и минералы симметричны относительно центральной линии,
# но границы уровней не совпадают, если h не делится нацело на LEVELS
# !! на данный момент высота поля выравнивается по LEVELS, чтобы упростить вычисления с эенергией !!
class Field:
    def __init__(self, height, width):
        self.turn = 0

        self.cells = {}         # Массив всех клеток на поле
        self.newid = 0
        self.field = []         # Содержимое поля
        self.W = width              # Ширина поля
        self._H = height // LEVELS * LEVELS     # Высота поля без учета стен, выровненная по количеству уровней
        self.H = self._H + 2    # Реальная высота - высота поля с учетом верхней и нижней стены

        # Сила солнца и количество минералов на кажом "энергоуровне"
        self.energy_levels = [(i if i > 0 else 0) for i in range(TOP_LVL, TOP_LVL - LEVELS, -1)]
        self.level_h = self._H // LEVELS        # Высота одного уровня

        # Заполняем поле пустотой + стены сверху и снизу (эти стены отображаться не будут)
        # Здесь так же можно поместить стены внутрь карты
        self.field.append([WALL] * self.W)
        self.field += [([EMPTY] * self.W) for _ in range(self._H)]       # [[EMPTY] * self.W] * self.H не работает
        self.field.append([WALL] * self.W)                               # т.к. получаются не копии списка а ссылки на него

        # Создаем первую клетку по центру поля
        first_x = self.H // 2
        first_y = self.W // 2
        self.putCell((first_x, first_y), init=INIT)

    # Отладочная функция, выводящая на экран поле и клетки
    def printMe(self):
        for line in self.field:
            print(line)
        for cell in self.cells:
            print(cell.genome)

    # Делаем очередной ход
    def hostTurn(self):
        for cell in list(self.cells.keys()):
            try:
                self.cells[cell].makeTurn()
            except KeyError:
                # Если клетку съели до того, как она успела сделать ход,
                # то в массиве её не будет и мы получим исключение.
                # Его можно просто проигнорировать
                pass
        self.turn += 1

    # Получаем, что находится в данной позиции
    def whatsThere(self, pos):
        x, y = pos

        # Учитываем зацикленность
        y = self.borderCycle(y)

        return self.field[x][y]

    # Получаем силу солнца в заданной позиции
    def howIsSun(self, x):
        return self.energy_levels[(x - 1) // self.level_h]

    # Получаем количество минералов в заданной позиции
    def howIsMineral(self, x):
        return self.energy_levels[(self._H - x) // self.level_h]

    # Помещаем новую клетку в заданную позицию (если там пусто)
    def putCell(self, pos, parent=None, multi=False, init=-1):
        x, y = pos
        y = self.borderCycle(y)
        if self.field[x][y] is EMPTY:
            _id = self.newid
            cell = Cell(self, _id, (x, y), parent, multi, init)
            self.cells[_id] = cell
            self.field[x][y] = _id
            self.newid = _id + 1

            return cell

    # Перемещаем клетку в новую позицию
    def moveCell(self, cell, new_pos):
        # Получаем старую и новую позицию
        x = cell.x
        y = cell.y
        new_x, new_y = new_pos

        new_y = self.borderCycle(new_y)     # Учитываем зацикленность

        # Перемещаем
        self.field[new_x][new_y] = self.field[x][y]
        self.field[x][y] = EMPTY

        # Сообщаем клетке, что она переехала
        cell.x = new_x
        cell.y = new_y

    # Убираем с поля клетку
    def disposeCell(self, cell):
        cell_ = self.cells.pop(cell)    # Достаем из списка

        # И узнаем координату
        x = cell_.x
        y = cell_.y
        cell_.die()     # Убеждаемся, что клетка мертва

        self.field[x][y] = EMPTY    # Убираем с поля

        return NUTRITION

    # Учитываем зацикленность поля
    def borderCycle(self, y):
        if y < 0:
            y = y + self.W
        if y >= self.W:
            y = y - self.W
        return y
