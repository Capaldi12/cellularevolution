"""
    Отрисовка поля и клеток на нем
"""

import pygame
from model import *
from functions import addColors


class Artist:
    def __init__(self, screen: pygame.Surface, field: Field, size, border=True):
        self.sc = screen
        self.field = field
        self.size = size
        # Выбор функции для отрисоки клетки
        self.drawCell = self.drawCell_bordered if border else self.drawCell_borderless

    def draw(self):
        self.drawBG()
        self.drawCells()

    # Отрисовка всех клеток
    def drawCells(self):
        for cell in self.field.cells.values():
            self.drawCell(cell)

    # Отрисовка клетки с чеными границами
    def drawCell_bordered(self, cell):
        x = cell.x - 1
        y = cell.y
        size = self.size
        x *= size
        y *= size
        if cell.dead:
            color = (100, 100, 100)
        else:
            color = cell.color
        pygame.draw.rect(self.sc, (0, 0, 0), (y, x, size, size))
        pygame.draw.rect(self.sc, color, (y+1, x+1, size-2, size-2))

    # Отрисовка клетки без границ
    def drawCell_borderless(self, cell):
        x = cell.x - 1
        y = cell.y
        size = self.size
        x *= size
        y *= size
        if cell.dead:
            color = (100, 100, 100)
        else:
            color = cell.color
        pygame.draw.rect(self.sc, color, (y, x, size, size))

    # Отрисовка фона
    def drawBG(self):
        levels = self.field.energy_levels
        h = self.field.level_h
        width = self.field.W
        size = self.size

        f = 12

        for i in range(len(levels)):
            r = levels[i] * f
            g = levels[i] * f
            b = 255 - levels[LEVELS - 1 - i] * f
            cl = addColors((r, g, b), (10, 10, 10))
            pygame.draw.rect(self.sc, cl, (0, h * size * i, width * size, h * size))

    # На данный момент работает не очень эффективно,
    # так как на каждой итерации необходимо высчитывать цвет каждой из полос.
    # Желательно как-либо сохранить просчитанный фон и отрисовывать его целиком (возможно Sprite?)
